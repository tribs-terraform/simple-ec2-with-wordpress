# Simple WordPress sur AWS EC2 avec Terraform et Ansible

Ce projet déploie une instance WordPress fraîchement installée et prête à être configurée sur une instance AWS EC2. Il peut être exécuté à partir d'une machine locale pour un déploiement distant ou via la pipeline GitLab.

## Prérequis

- Terraform
- Ansible
- Compte AWS
- (Optionnel) GitLab pour l'exécution via la pipeline

## Déploiement

### Exécution Locale pour un Déploiement Distant

1. Configurez vos identifiants AWS.
2. Configurez la clé SSH, l'utilisateur SSH et l'hôte SSH dans le fichier `terraform.tfvars` ou utilisez les variables d'environnement `SSH_PRIVATE_KEY`, `SSH_USER`, et `SSH_HOST`.
3. Exécutez `terraform init` pour initialiser le projet.
4. (Optionnel) Exécutez `terraform plan` pour visualiser les changements avant l'application.
5. Exécutez `terraform apply` pour déployer l'instance WordPress sur EC2.

### Exécution via GitLab pour un Déploiement Distant

1. Configurez les variables `SSH_PRIVATE_KEY`, `SSH_USER`, et `SSH_HOST` dans les variables CI/CD du projet GitLab ou utilisez le fichier `gitlab-ci.yml` et/ou le fichier `default_ssh_key` pour la clé SSH.
2. Exécutez la pipeline GitLab pour déployer le projet sur EC2.

## Configuration

Accédez à l'URL de l'instance EC2 pour configurer WordPress.

## Configuration de la Clé SSH et des Identifiants SSH

- **Exécution Locale**: Configurez la clé SSH, l'utilisateur SSH et l'hôte SSH dans le fichier `terraform.tfvars`.
- **Exécution via GitLab**: Configurez les variables `SSH_PRIVATE_KEY`, `SSH_USER`, et `SSH_HOST` dans les variables CI/CD du projet GitLab, ou utilisez le fichier `gitlab-ci.yml` et/ou le fichier `default_ssh_key` pour la clé SSH.

## Support

Pour toute question ou support, veuillez ouvrir une issue.

## Licence

Ce projet est sous licence MIT.
