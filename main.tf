variable "ssh_host" {}
variable "ssh_user" {}
variable "ssh_key" {}
variable "gitlab_ssh_key" {
  description = "Optional SSH private key from GitLab"
  type        = string
  default     = ""
}

locals {
  ssh_private_key = var.gitlab_ssh_key != "" ? var.gitlab_ssh_key : file(var.ssh_key)
  ssh_user = var.gitlab_ssh_user != "" ? var.gitlab_ssh_user : var.ssh_user
  ssh_host = var.gitlab_ssh_host != "" ? var.gitlab_ssh_host : var.ssh_host
}

resource "null_resource" "install_ansible" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = fileexists("~/.ssh/id_rsa") ? file("~/.ssh/id_rsa") : local.ssh_private_key
	agent       = true
  }

  provisioner "local-exec" {
    command = "ssh -v -i ~/.ssh/id_rsa ${var.ssh_user}@${var.ssh_host}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update -qq >/dev/null",
      "sudo apt install -y software-properties-common",
      "sudo apt-add-repository --yes --update ppa:ansible/ansible",
      "sudo apt install -y ansible",
      "sudo apt install -y python3-docker" # Utilisez le package système pour la bibliothèque Docker
    ]
  }

  provisioner "file" {
    source      = "ansible-playbook.yml"
    destination = "/tmp/ansible-playbook.yml"
  }

  provisioner "file" {
    source      = "startup-options.conf"
    destination = "/tmp/startup-options.conf"
  }

  provisioner "file" {
    source      = "docker-compose.yml"
    destination = "/tmp/docker-compose.yml"
  }

  provisioner "remote-exec" {
    inline = [
      "ansible-playbook /tmp/ansible-playbook.yml"
    ]
  }
}

output "host" {
  value = var.ssh_host
}

output "user" {
  value = var.ssh_user
}

output "private_key" {
  value = var.ssh_key
}
